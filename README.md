

# Loguser Laravel Api
- Laravel v5.6.39

## Install Loguser
- Clone repository from https://bitbucket.org/nmlleitao/log-user/src/master/
- In \log-user run command: Composer Update
- create a DB schema named "loguser"
- set your DB user and passoword in .env file (create .env file from .env.example file)
- run command: php artisan migrate

## Run Tests
- run command: composer phptest

## Run Scripts
- Use one api tester application like Postman
- run command: php artisan serve
- run Create new User script: POST method on http://127.0.0.1:8000/api/new?name=User Test&email=user@test.pt&birthday=1999-12-12&gender=1
- run List Users script: GET method on http://127.0.0.1:8000/api/users
- run List one User details script: GET method on http://127.0.0.1:8000/api/user/1
- run Update User script: PUT method on http://127.0.0.1:8000/api/update/1?name=User2 Test&email=user2@test.pt&birthday=1999-12-22&gender=0
- run Delete User script: DELETE method on http://127.0.0.1:8000/api/delete/1
