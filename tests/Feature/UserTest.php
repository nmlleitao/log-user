<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{

	/**
     * Test to store new user.
     *
     * @return void
     */
    public function testCreateNewUser()
    {
       
        $payload = [
        	'name' => 'User Test',
		    'email' => 'user@test.pt',
		    'birthday' => '1999-12-12',
		    'gender' => 1
		];

        $this->post('api/new', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                    'id',
                    'name',
                    'email',
                    'birthday',
                    'gender',
                    'created_at',
                    'updated_at',
            ]);
    }

	/**
     * Test to get all users.
     *
     * @return void
     */
    public function testListUsers()
    {
    	$response = $this->get('api/users');

        $response->assertStatus(200);
 
    }

    /**
     * Test to get user detail.
     *
     * @return void
     */
    public function testDetailUser()
    {
    	$user = User::first();

    	$response = $this->get('api/user/'.$user->id);
        $response->assertStatus(200);
 
    }

    /**
     * Test to update user.
     *
     * @return void
     */
    public function testUpdateUser()
    {
        $user = User::first();

        $payload = [
        	'name' => 'User2 Test',
		    'email' => 'user2@test.pt',
		    'birthday' => '1999-12-22',
		    'gender' => 0
		];

        $this->put('api/update/'.$user->id, $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                    'id',
                    'name',
                    'email',
                    'birthday',
                    'gender',
                    'created_at',
                    'updated_at',
            ]);
    }


    /**
     * Test to delete user.
     *
     * @return void
     */
    public function testDeleteUser()
    {
    	$user = User::first();

    	$response = $this->delete('api/delete/'.$user->id);
        $response->assertStatus(204);
 
    }


    
}
