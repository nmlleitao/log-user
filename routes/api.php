<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/users', 'Api\UserController@list');
Route::get('/user/{id}', 'Api\UserController@detail');
Route::post('/new', 'Api\UserController@store');
Route::put('/update/{id}', 'Api\UserController@update');
Route::delete('/delete/{id}', 'Api\UserController@delete');

